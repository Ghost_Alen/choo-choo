#include <iostream> 
#include <ctime> 
#include <stdio.h> 
using namespace std;
void CreateArray(int AmountOfSportsmen, int TimeOfSportsmen[]);
void PrintArray(int AmountOfSportsmen, int TimeOfSportsmen[]);
void FindMaxTime(int AmountOfSportsmen, int TimeOfSportsmen[]);
void FindMinTime(int AmountOfSportsmen, int TimeOfSportsmen[]);
void FindMiddleTime(int AmountOfSportsmen, int TimeOfSportsmen[]);
void FindMaxResult(int AmountOfSportsmen, int TimeOfSportsmen[]);

int main()
{
	setlocale(LC_ALL, "rus");
	int AmountOfSportsmen, TimeOfSportsmen1, TimeOfSportsmen2, i;

	srand(time(0));
	AmountOfSportsmen = 25 + rand() % 26;
	cout << "Amount Of Sportsmen = " << AmountOfSportsmen << endl;
	int *TimeOfSportsmen;
	TimeOfSportsmen = new int[AmountOfSportsmen];
	CreateArray(AmountOfSportsmen, TimeOfSportsmen);
	PrintArray(AmountOfSportsmen, TimeOfSportsmen);
	FindMinTime(AmountOfSportsmen, TimeOfSportsmen);
	FindMaxTime(AmountOfSportsmen, TimeOfSportsmen);
	FindMiddleTime(AmountOfSportsmen, TimeOfSportsmen);
	FindMaxResult(AmountOfSportsmen, TimeOfSportsmen);
	system("pause");
	return 0;
}
void FindMinTime(int AmountOfSportsmen, int TimeOfSportsmen[])
{
	int min, min1, min2;
	min = TimeOfSportsmen[1];
	for (int i = 1; i <= AmountOfSportsmen; i++)
	{
		if (TimeOfSportsmen[i] < min)
		{
			min = TimeOfSportsmen[i];
		}
		min1 = min / 60;
		min2 = min % 60;
	}
	cout << "  Min Time = " << min1 << ":" << min2 << endl;
}
void FindMaxTime(int AmountOfSportsmen, int TimeOfSportsmen[])
{
	int max, max1, max2;
	max = TimeOfSportsmen[1];
	for (int i = 1; i <= AmountOfSportsmen; i++)
	{
		if (TimeOfSportsmen[i] > max)
		{
			max = TimeOfSportsmen[i];
		}
		max1 = max / 60;
		max2 = max % 60;
	}
	cout << "  Max time = " << max1 << ":" << max2 << endl;

}

void CreateArray(int AmountOfSportsmen, int TimeOfSportsmen[])
{
	int TimeOfSportsmen1, TimeOfSportsmen2;
	for (int i = 1; i <= AmountOfSportsmen; i++)
		TimeOfSportsmen[i] = 30 + rand() % 151;
}

void FindMiddleTime(int AmountOfSportsmen, int TimeOfSportsmen[])
{
	int sr = 0, sr1, sr2;
	for (int i = 1; i <= AmountOfSportsmen; i++)
	{
		sr = (sr + TimeOfSportsmen[i]) / AmountOfSportsmen;
	}
	sr1 = sr / 60;
	sr2 = sr % 60;
	cout << " Middle time = " << sr1 << ":" << sr2 << endl;
}
void PrintArray(int AmountOfSportsmen, int TimeOfSportsmen[])
{
	int TimeOfSportsmen1, TimeOfSportsmen2;
	for (int i = 1; i <= AmountOfSportsmen; i++)
	{
		TimeOfSportsmen1 = TimeOfSportsmen[i] / 60;
		TimeOfSportsmen2 = TimeOfSportsmen[i] % 60;
		cout << " Sportsmen time " << i << ",  = " << TimeOfSportsmen[i] << "   " << TimeOfSportsmen1 << ":" << TimeOfSportsmen2 << endl;

	}
}
void FindMaxResult(int AmountOfSportsmen, int TimeOfSportsmen[])
{
	int Minutes = -1, Seconds = -1, TimeOfSportsmen1, TimeOfSportsmen2;

	while ((Minutes < 0 || Minutes>59) || (Seconds < 0 || Seconds>59))
	{
		cout << "Enter full amount of minutes: " << endl;
		cin >> Minutes;
		cout << "Enter full amount of seconds: " << endl;
		cin >> Seconds;

	}
	cout << "Results, more then " << Minutes << ":" << Seconds << endl;
	for (int i = 1; i <= AmountOfSportsmen; i++)
	{
		int TimeOfSportsmen1, TimeOfSportsmen2;
		TimeOfSportsmen1 = TimeOfSportsmen[i] / 60;
		TimeOfSportsmen2 = TimeOfSportsmen[i] % 60;

		if ((TimeOfSportsmen1 > Minutes) || ((TimeOfSportsmen1 >= Minutes) && (TimeOfSportsmen2 > Seconds)))

			cout << TimeOfSportsmen1 << ":" << TimeOfSportsmen2 << endl;
	}
}